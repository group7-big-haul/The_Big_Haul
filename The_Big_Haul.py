import sys
import time
import pyfiglet
import random

class Game:
    def __init__(self, participant):
        self.participant = participant
        self.life = 3
        self.bank = 0
        

    def correct(self):
        if player.bank == 0:
            setattr(player, "bank", 100)
        else:
            setattr(player, "bank", (self.bank*2))
        print(f'your new bank total is: ${player.bank}')
        if self.bank >= 102400:
            time.sleep(2)
            print(f'you have reached the max bank capacity which means...')
            time.sleep(2)
            print(pyfiglet.figlet_format("YOU  WIN !"))
            time.sleep(2)
            print(f'It ha been so good having you come on the show today {player.participant}')
            time.sleep(2)
            print(f'dont be shy, come back anytime')
            setattr(player, "bank", 0)
            sys.exit()

    def incorrect(self):
        setattr(player, "life", player.life-1)
        print(f'you have {player.life} lives remaining!')
        if self.life == 0:
            print(pyfiglet.figlet_format("GAME  OVER !"))
            setattr(self, "bank", 0)
            time.sleep(2)
            print(f'well unfortunately {player.participant} you have run out of lives...')
            time.sleep(2)
            print(f'your new bank total is ${player.bank}')
            time.sleep(2)
            print(f'unfortunately our time together has come to an end')
            time.sleep(2)
            print(f"we hope you'll join us again on THE BIG HAUL")
            #rip
            sys.exit()
 

def round_1_question(file_q, q_num):
    with open(file_q) as file:
        options = []
        lines = file.read().split("\n")
        for line in lines:
            options.append(line)
    question = options[0]
    answer = options[1]
    choices = options[1:]
    random.shuffle(choices)
    a = choices[0]
    b = choices[1]
    c = choices[2]
    d = choices[3]
    print("Question " + q_num)
    print("-----------------------------------")
    print(question)
    print("A: " + a)
    print("B: " + b)
    print("C: " + c)
    print("D: " + d)
   
    selection = {"A":choices[0], 'B': choices[1], 'C':choices[2], 'D':choices[3]}
    usr_inp = input("Type your answer: ").upper()
    
    while usr_inp not in selection.keys():
        print("")
        print("A: " + a)
        print("B: " + b)
        print("C: " + c)
        print("D: " + d)
        usr_inp = input("please pick from the options availible: ").upper()
        
        
    if selection.get(usr_inp) == answer:
        time.sleep(2)
        print("correct!")
        time.sleep(2)
        print("well done")
        time.sleep(2)
        player.correct()
    else:
        time.sleep(2)
        print("incorrect :(")
        time.sleep(2)
        print("better luck next time")
        time.sleep(2)
        player.incorrect()
#Open file set values
def round_2_question(file_q, q_num):
    with open(file_q) as file:
        options = []
        lines = file.read().split("\n")
        for line in lines:
            options.append(line)
    old_comp = options[3]
    value_old = options[4]
    question = options[0]
    new_comp = options[1]
    value_new = options[2]
    with open("round2/v-old.txt") as filev: #Opens file and updates new values with the old values
        rows =[]
        row = filev.read().split("\n")
        for value in row:
            rows.append(value)
        if len(rows) >= 2:
            value_old = rows[0]
            old_comp = rows[1]
        filev.close()
    if old_comp == new_comp:
        old_comp = options[3]
        value_old = options[4]
    print("Question " + q_num)
    print("-----------------------------------")
    print(question + old_comp + " ?")
    print("Higher")
    print("Lower")
    print("Draw")
   
   #Available options
    selection = ["Higher", "Lower", "Draw"]
    usr_inp = input("Type your answer: ")

    #Sets the answer
    if value_new < value_old:
        answer = "Lower"
    elif value_new > value_old:
        answer = "Higher"
    else:
        answer = "Draw"

    #Checks if the input is part of the options available
    while usr_inp not in selection:
        print("")
        print("The answer is case sensitive you spelled: " + usr_inp)
        print("Higher")
        print("Lower")
        usr_inp = input("please pick from the options availible: ")

    #If user is correct saves the values to file and runs correct function
    if usr_inp == (answer):
        old_comp = new_comp
        value_old = value_new
        with open("round2/v-old.txt") as filev:
            line = filev.read().split("\n")
            filev.close()
        for q in line:
            if len(q) >= 2:
                f = open("round2/v-old.txt","w")
                f.close()
        with open("round2/v-old.txt", 'a') as filev:
            filev.write(value_old + "\n")
            filev.write(old_comp + "\n")
            filev.close()
            time.sleep(2)
            print("correct!")
            time.sleep(2)
            print("well done")
            time.sleep(2)
            player.correct()
    #If User is wrong updates values and runs incorrect function
    else:
        old_comp = new_comp
        value_old = value_new
        with open("round2/v-old.txt") as filev:
            line = filev.read().split("\n")
            filev.close()
        for q in line:
            if len(q) >= 2:
                f = open("round2/v-old.txt","w")
                f.close()
        with open("round2/v-old.txt", 'a') as filev:
            filev.write(value_old + "\n")
            filev.write(old_comp + "\n")
            filev.close()
        time.sleep(2)
        print("incorrect :(")
        time.sleep(2)
        print("better luck next time")
        time.sleep(2)
        player.incorrect()

player = Game("me")
print("Helloooo ladies and gentlemen")
time.sleep(2)
print("my name is Johnny Jackpot and welcome to...")
time.sleep(4)
print(pyfiglet.figlet_format(" THE  BIG  HAUL !"))
time.sleep(2)
print("Today I am joined by our very special contestant")
time.sleep(3)
cont_name = input("Tell me, what is your name? ").capitalize()
while not cont_name.strip():
    cont_name = input("Please enter a valid name: ")
player = Game(cont_name)
print("It's a pleasure to meet you " + player.participant)
time.sleep(3)
address = input("and where are you from " + player.participant + "? ").capitalize()
while not address.strip():
    address = input("Please enter a valid address: ")
print(address + " really... I heard that place is a bit of a kip")
time.sleep(2)
ready = input("Now " + cont_name + " from " + address + " are you ready? If so I wanna hear you say YESSIR! ")
while ready.upper() != "YESSIR":
    ready = input("I can't hear you... ")
time.sleep(2)
print("THEN IT'S ONTO THE FIRST ROUND!")
time.sleep(2)


#FIRST ROUND
round1_questions = ["Q1.txt", "Q2.txt", "Q3.txt", "Q4.txt", "Q5.txt", "Q6.txt", "Q7.txt", "Q8.txt", "Q9.txt", "Q10.txt", "Q11.txt", "Q12.txt"]

with open("round1/r-hist.txt") as fileh:
    line = fileh.read().split("\n")
    fileh.close()
for q in line:
    if len(line) >= 11:
        f = open("round1/r-hist.txt","w")
        f.close()
    elif q != "":
        round1_questions.remove(q)
random_choice = random.choice(round1_questions)

loop = 1
while loop < 5:
    round_1_question(f"round1/{random_choice}", str(loop))
    round1_questions.remove(random_choice)
    with open("round1/r-hist.txt", "a") as fileh:
        fileh.write(random_choice + "\n")
        fileh.close()
    random_choice = random.choice(round1_questions)
    loop += 1
    time.sleep(2)
    print("")

#Second ROUND
round2_questions = ["Q1.txt", "Q2.txt", "Q3.txt", "Q4.txt", "Q5.txt", "Q6.txt", "Q7.txt", "Q8.txt", "Q9.txt", "Q10.txt", "Q11.txt", "Q12.txt"]

with open("round2/r-hist.txt") as fileh:
    line = fileh.read().split("\n")
    fileh.close()
for q in line:
    if len(line) >= 11:
        f = open("round2/r-hist.txt","w")
        f.close()
    elif q != "":
        round2_questions.remove(q)
random_choice = random.choice(round2_questions)

loop = 1
while loop < 5:
    round_2_question(f"round2/{random_choice}", str(loop))
    round2_questions.remove(random_choice)
    with open("round2/r-hist.txt", "a") as fileh:
        fileh.write(random_choice + "\n")
        fileh.close()
    random_choice = random.choice(round2_questions)
    loop += 1
    time.sleep(2)
    print("")
